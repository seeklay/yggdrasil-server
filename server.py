"""
    Yggdrasil server
    Mojang Auth Server
    Minecraft Session Server
    FastAPI RW w/ sqlAMY
"""

from fastapi import FastAPI, Request

from fastapi.responses import PlainTextResponse
from starlette.responses import FileResponse

from fastapi.exceptions import RequestValidationError
from starlette.exceptions import HTTPException

from fastapi.staticfiles import StaticFiles

from fastapi_sqlalchemy import DBSessionMiddleware  # middleware helper
from fastapi_sqlalchemy import db

from sqlalchemy import create_engine, select
import models

import os
import logging
from util import CustomFormatter

logger = logging.getLogger("yggdrasil")
stdout_handler = logging.StreamHandler()
stdout_handler.setLevel(logging.DEBUG)
stdout_handler.setFormatter(CustomFormatter('[%(asctime)s] [%(levelname)s] %(name)s:  %(message)s'))
logger.addHandler(stdout_handler)
logger.setLevel(logging.DEBUG if "YGGDRASIL_DEBUG_LOGGING" in os.environ else logging.INFO)

engine = create_engine("sqlite:///players.db")
models.Base.metadata.create_all(engine)

app = FastAPI()
app.add_middleware(DBSessionMiddleware, db_url="sqlite:///players.db")
app.logger = logger

app.autoreg = True
app.autoreg_country = "IE"
app.autoreg_language = "en-us"
app.autoreg_texture = "2e61fbaefbb68dcfd522e93452eea379ffb6ca32537bcaaa0db915373f5d85c0"

app.admin_password = "admin"
app.allow_falsify = False

import authserver
app.include_router(authserver.router)

import sessionserver
app.include_router(sessionserver.router)

import frontend
app.include_router(frontend.router)

@app.on_event("startup")
async def startup():
    logger.info("people stuck at windows...")

@app.exception_handler(404)
async def not_found_exception_handler(request: Request, exc: HTTPException):
    return PlainTextResponse(f"Scream if you wanna, shout if you need\n{request.url}", status_code=404)

@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exception: HTTPException):
    return PlainTextResponse(f"Bad Request to {request.url}\n{exception}", status_code=400)

@app.get("/")
async def index():
    return FileResponse('index.htm')
