import logging, os
os.system('color')

class CustomFormatter(logging.Formatter):    
    

    debug_rgb = ('246', '229', '141')
    DEBUG = f"\x1b[38;2;{';'.join(debug_rgb)}m"

    INFO = '\x1b[38;5;39m'
    WARNING = '\x1b[38;5;226m'
    ERROR = '\x1b[38;5;196m'
    CRITICAL = '\u001b[31m'

    reset = '\x1b[0m'

    def __init__(self, fmt):
        super().__init__()
        self.fmt = fmt
        self.FORMATS = {
            logging.DEBUG: self.DEBUG + self.fmt + self.reset,
            logging.INFO: self.INFO + self.fmt + self.reset,
            logging.WARNING: self.WARNING + self.fmt + self.reset,
            logging.ERROR: self.ERROR + self.fmt + self.reset,
            logging.CRITICAL: self.CRITICAL + self.fmt + self.reset
        }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt, datefmt='%m/%d/%Y %I:%M:%S %p')
        return formatter.format(record)
