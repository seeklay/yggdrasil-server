import uvicorn
import logging

if __name__ == "__main__":    
    uvicorn.run("server:app", host="0.0.0.0", port=443, ssl_keyfile="x509/cert.key", ssl_certfile="x509/fullchain.crt", log_level=logging.CRITICAL, reload=False, workers=1)
