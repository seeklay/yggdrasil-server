from fastapi import APIRouter, Form, Request
from fastapi_sqlalchemy import db  # an object to provide global access to a database session
from pydantic import BaseModel
from fastapi.responses import PlainTextResponse

from models import Player

import datetime

import uuid

router = APIRouter()

@router.post("/signup")
async def sign_up(username: str = Form(), password: str = Form(), country: str = Form()):
    player = db.session.query(Player).where(Player.username == username).first()
    if player:
        return PlainTextResponse("Player already exists!", status_code=400)
    
    if len(username) > 16:
        return text("Player username length must be <= 16", status_code=400)

    if len(password) > 32:
        return text("Player password length must be <= 32!", status_code=400)

    player = Player(
        uuid = str(uuid.uuid4()),
        username = username,
        password = password,
        banned = False,
        singleplayer = False,
        preferredLanguage = "en-us",
        registrationCountry = country,
        joined_at = datetime.datetime.now(),
        texture_id = "2e61fbaefbb68dcfd522e93452eea379ffb6ca32537bcaaa0db915373f5d85c0"
    )
    
    db.session.add(player); db.session.commit()
    return PlainTextResponse(f'Done! New user uuid: {player.uuid}')

@router.post("/signdown")
async def del_user(username: str = Form(), password: str = Form()):
    player = db.session.query(Player).where(Player.username == username).first()
    if not player:
        return PlainTextResponse(f"Player with username {username} doesn't exist!", status_code=400)
    if player.password != password:
        return PlainTextResponse(f"Invalid password!", status_code=403)

    db.session.delete(player); db.session.commit()
    return PlainTextResponse('Successful!', status_code=200)

@router.post("/ban")
async def del_user(request: Request, username: str = Form(), admin_password: str = Form()):
    player = db.session.query(Player).where(Player.username == username).first()
    if not player:
        return PlainTextResponse(f"Player with username {username} doesn't exist!", status_code=400)    
    
    if request.app.admin_password != admin_password:
        return text(f"Invalid admin password!", status_code=403)    
    
    player.banned = not player.banned
    db.session.commit()
    return PlainTextResponse(f"Player was successfully {'un' if not player.banned else ''}banned", status_code=200)

@router.get("/players")
async def players():
    players = db.session.query(Player)
    response = "Players:\n"    
    for player in players:
        response += "\n"
        response += f"  Username: {player.username}\n"
        response += f"  UUID: {player.uuid}\n"
        response += f"  Country: {player.registrationCountry}\n"
        response += f"  Language: {player.preferredLanguage}\n"
        response += f"  Banned: {player.banned}\n"
        response += f"  Member since: {player.joined_at} (joined {(datetime.datetime.now() - player.joined_at).days} days ago)\n"
    
    return PlainTextResponse(response)

@router.get("/player")
async def show_player(username: str):
    player = db.session.query(Player).where(Player.username == username).first()
    if not player:
        return PlainTextResponse(f"Player with username {username} doesn't exist!", status_code=400)    

    response = f"Username: {player.username}\n"
    response += f"UUID: {player.uuid}\n"
    response += f"Country: {player.registrationCountry}\n"
    response += f"Language: {player.preferredLanguage}\n"
    response += f"Banned: {player.banned}\n"
    response += f"Singleplayer: {player.singleplayer}\n"
    response += f"Member since: {player.joined_at}\n"

    return PlainTextResponse(response)
