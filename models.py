from sqlalchemy import ForeignKey, String, Boolean, DateTime
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship
from sqlalchemy.types import TEXT
import datetime
import uuid
from typing import List

class Base(DeclarativeBase):
    pass

class Player(Base):
    __tablename__ = "players"

    uuid: Mapped[str] = mapped_column(TEXT(36), primary_key=True)
    username: Mapped[str] = mapped_column(String(16))
    password: Mapped[str] = mapped_column(String(64))

    banned: Mapped[bool] = mapped_column(Boolean)
    singleplayer: Mapped[bool] = mapped_column(Boolean)

    preferredLanguage: Mapped[str] = mapped_column(TEXT(5))
    registrationCountry: Mapped[str] = mapped_column(TEXT(2))    
    joined_at: Mapped[datetime.datetime] = mapped_column(DateTime)

    texture_id: Mapped[str] = mapped_column(TEXT())

    tokens: Mapped[List["Token"]] = relationship(
        back_populates="player", cascade="all, delete-orphan"
    )

    sessions: Mapped[List["Session"]] = relationship(
        back_populates="player", cascade="all, delete-orphan"
    )

class Token(Base):
    __tablename__ = "tokens"

    accessToken: Mapped[str] = mapped_column(TEXT(36), primary_key=True)
    clientToken: Mapped[str] = mapped_column(TEXT(36))
    created_at: Mapped[datetime.datetime] = mapped_column(DateTime)
    player_uuid: Mapped[int] = mapped_column(ForeignKey("players.uuid"))
    player: Mapped["Player"] = relationship(back_populates="tokens")

class Session(Base):
    __tablename__ = "session"
    
    serverId: Mapped[str] = mapped_column(String(30), primary_key=True)
    selectedProfile: Mapped[str] = mapped_column(String(30))
    created_at: Mapped[datetime.datetime] = mapped_column(DateTime)
    player_uuid: Mapped[int] = mapped_column(ForeignKey("players.uuid"))
    player: Mapped["Player"] = relationship(back_populates="sessions")

    @classmethod
    def purge(cls, session):
        count = 0
        for s in session.query(cls):
            if (datetime.datetime.now() - s.created_at).seconds > 7:
                session.delete(s)
                count += 1
        return count
