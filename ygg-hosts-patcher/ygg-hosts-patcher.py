"""
    Small PyQt5 GUI for patching windows hosts file.
    It points mojang authserver and sessionserver
    hosts to selected IP addr (default localhost)
    
    Author:
        seeklay <rudeboy at seeklay.icu>
    
    Year:
        Twenty Twenty Three
    
    Operating System:
        Microsoft Windows 7 and above
"""
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QPushButton, QLineEdit, QLabel, QApplication, QWidget, QWidget, QFormLayout
import ctypes, sys, re

from typing import List, Tuple

SEMVER = "0.1"

def is_admin():
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False

class HostsManipulator:

    @staticmethod
    def read_hosts():
        """
            Read hosts file content
        """
        with open("C:\Windows\System32\drivers\etc\hosts", "r", encoding="utf-8") as f:
            return f.read()
    
    @staticmethod
    def write_hosts(hosts: str):
        """
            Rewrite hosts file with new content
        """
        with open("C:\Windows\System32\drivers\etc\hosts", "w", encoding="utf-8") as f:
            f.write(hosts)

    @staticmethod
    def get_mojang_hosts():
        """
            Get all mojang.com hosts from hosts file
        """
        hosts = HostsManipulator.read_hosts()
        return re.findall("(.*) ([a-z]+.mojang.com)", hosts)
    
    @staticmethod
    def append_hosts(hosts_to_append: List[Tuple[str, str]]):
        """
            Append hosts file with new hosts
        """
        with open("C:\Windows\System32\drivers\etc\hosts", "a", encoding="utf-8") as f:
            for host in hosts_to_append:
                f.write(f"\n{host[0]} {host[1]}")

class App(QWidget):

    def __init__(self):
        super().__init__()
        self.setWindowTitle(f"Mojang hosts patcher for windows v{SEMVER}")
        
        self.ip_addr_input = QLineEdit("127.0.0.1", self)
        self.ip_addr_input.setFont(QFont("Open Sans", 22))

        self.localhost_button = QPushButton('D', self)
        self.localhost_button.setToolTip('Reset address field to localhost')
        self.localhost_button.setFont(QFont("Open Sans", 21))
        self.localhost_button.setStyleSheet("QPushButton{color: gray;}")
        self.localhost_button.clicked.connect(self.localhost_button_handler)

        self.patch_button = QPushButton('Patch hosts', self)
        self.patch_button.setToolTip('Point mojang hosts to selected address')
        self.patch_button.setFont(QFont("Open Sans", 22))
        self.patch_button.clicked.connect(self.patch_button_handler)
        
        self.flush_button = QPushButton('Flush mojang hosts', self)
        self.flush_button.setToolTip('Remove all mojang hosts from hosts file')
        self.flush_button.setFont(QFont("Open Sans", 22))
        self.flush_button.clicked.connect(self.flush_button_handler)

        self.status_label = QLabel(self.get_mojang_hosts_status(), self)
        self.status_label.setToolTip('Patch status')
        self.status_label.setFont(QFont("Open Sans", 17))

        flo = QFormLayout()
        flo.addRow(self.ip_addr_input)
        flo.addRow(self.patch_button)
        flo.addRow(self.flush_button)
        flo.addRow(self.status_label)
        self.setLayout(flo)
        self.show()

    @staticmethod
    def get_mojang_hosts_status():
        """Get all mojang hosts names and addrs"""
        mojang_hosts = HostsManipulator.get_mojang_hosts()
        return "Current config:\n" + ("\n".join([f"{i[0]} {i[1]}" for i in mojang_hosts]) if mojang_hosts else "NOT PATCHED")

    def resizeEvent(self, event):
        """Move ip address reset button to the end of input"""
        iy = self.ip_addr_input.pos().y()
        ix = self.ip_addr_input.pos().x()
        ai = self.ip_addr_input.frameGeometry()
        self.localhost_button.setGeometry(ai.width()+iy-ai.height(), ix, ai.height(), ai.height())

    def localhost_button_handler(self):
        """Reset ip address input to the localhost value"""
        self.ip_addr_input.setText("127.0.0.1")
    
    def patch_button_handler(self):
        """Patch hosts file with mojang hosts"""
        self.flush_button_handler()
        ip_addr = self.ip_addr_input.text()

        mojang_hosts = [
            (ip_addr, "authserver.mojang.com"),
            (ip_addr, "sessionserver.mojang.com")
        ]
        HostsManipulator.append_hosts(mojang_hosts)

        self.status_label.setText(self.get_mojang_hosts_status())

    def flush_button_handler(self):
        """Remove mojang hosts from hosts file"""
        hosts = HostsManipulator.read_hosts()
        new_hosts = re.sub("(.*) [a-z]+.mojang.com", "", hosts).rstrip()
        HostsManipulator.write_hosts(new_hosts)

        self.status_label.setText(self.get_mojang_hosts_status())

if __name__ == '__main__':
    if not is_admin():
        ctypes.windll.user32.MessageBoxW(None, 'Run as admininstator', 'Elevation required!', 0)
        sys.exit(1)

    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
