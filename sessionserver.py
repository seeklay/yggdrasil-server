from fastapi import APIRouter, Request, Response, status
from fastapi.responses import JSONResponse

from pydantic import BaseModel
from typing import Optional

from fastapi_sqlalchemy import db

from models import Player, Token, Session
from uuid import UUID
import datetime
import base64, json

from Crypto.Hash import SHA1
from Crypto.Signature import pkcs1_15
from Crypto.PublicKey import RSA

router = APIRouter()

class Join(BaseModel):
    accessToken: str
    selectedProfile: str
    serverId: str

class Falsify(BaseModel):
    their: str
    our: str

with open("private.pem") as f:
    privkey = RSA.import_key(f.read())

@router.post("/session/minecraft/join")
async def join(req: Request, body: Join):
    req.app.logger.debug(body)
    '''Session joining'''
    token = db.session.query(Token).where(Token.accessToken == body.accessToken).first()
    if not token:
        response = {
            "error": "ForbiddenOperationException",
            "errorMessage": "Invalid token."
        }
        
        return JSONResponse(response, status_code=403)

    if UUID(token.player.uuid) != UUID(body.selectedProfile):
        response = {
            "error": "ForbiddenOperationException",
            "errorMessage": "Invalid profile!"
        }

        return JSONResponse(response, status_code=403)

    if token.player.banned:
        response = {
            "error": "UserBannedException",
            "path": "/session/minecraft/join",
            "errorMessage": "\nYou were banned in multiplayer!"
        }
        req.app.logger.debug(f"[{token.player.username}] Tried to join game session but he is banned!")
        return JSONResponse(response, status_code=403)

    if token.player.singleplayer:
        response = {
            "error": "InsufficientPrivilegesException",
            "path": "/session/minecraft/join",            
            "errorMessage": "\nYou are a singleplayer!"
        }
        req.app.logger.debug(f"[{token.player.username}] Tried to join game session but he is a singleplayer!")
        return JSONResponse(response, status_code=403)
    
    session = Session(
        serverId = body.serverId,
        selectedProfile = body.selectedProfile,
        created_at = datetime.datetime.now(),
        player = token.player
    )
    pc = Session.purge(db.session)
    req.app.logger.debug(f"Deleted {pc} broken sessions")
    db.session.add(session); db.session.commit()
    
    req.app.logger.info(f"[{token.player.username}] requested to join game session with serverId: {body.serverId}")
    return Response(status_code=204)

@router.get("/session/minecraft/hasJoined")
async def has_joined(req: Request, username: str, serverId: str, ip: Optional[str] = None):
    session = db.session.query(Session).where(Session.serverId == serverId).first()

    if not session:
        response = {
            "error": "FailedToVerifyUsername",
            "path": "/session/minecraft/hasJoined"
        }
        return JSONResponse(response, status_code=403)
    
    if session.player.username != username:
        response = {
            "error": "FailedToVerifyUsername",
            "path": "/session/minecraft/hasJoined"
        }
        return JSONResponse(response, status_code=403)

    db.session.delete(session)
    db.session.commit()

    skin = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAF/klEQVR42u1bS2skVRTufyEjKg66K3qrAzKzUXQ3YBYGxsCQRQeULOIgUTIL8THBjbgYcOciC8GFi2mcURHDEBdOFi4iMZloMqHTnQQGDYWRRCav6/1u11e5dfvWq6s6Xa114KPOfVTVPd8999WnulKJkeHhYTE+Pi6mpqbEyMiI0icnJ5U+OjoqpmuPi3evPKbwxfWnVfrTN59Q+OqD86KSUZxVVzju1xJXxNuuIxYk3HsSNyScqogrz/p+RcDY2JgVExMTymgYenv6mYBOQgaeAPRyGAEg5/nqs+KlC1Xx6ovPiauXLylARx7KBp4AuDsNpugeoBv/xmsvK+gkVLKLiEFvZWhoKJQAkAMjW998aEVOBPRX0MtRQyDMeGLgCdCHgI2ATWlkKL7/ZPAJwPKHiRDGwhswJLgsIh1JwCB4AIwDYBiu6HEYRsOh02CQAEBnmoRA57OQRj7r4xl4LvL1ckDN4h7CJrXP3npSEEkmTcz+AdxorwxYIbBSYMVQKwfeicbY3BsN5uaHeWjA+6PnAqsAyvWJUgfyo4YQ7o8jgO9MQUI6AtjDaCgaDIOgQ1Cmu77e43qPQnDFvQC9h8MEV9QnqfQM1E1CAHBychJAbgSIEMFW9r2r5/xex3jWe4I66mD3160kHQJp9g2pCODWFdfP33lKGQRwP4+Xb29vi6HLr/gkQEcetrvY+/MepHkft8MAywHk4x6eGZIQkHbjlJoANAQNReNAAoB9PPJgMOTiCxf8mR06BOSgHsBnwDAeikgAnocr3kVCWL8wBKDB1AE02r60fdSRR9LYs3gOe5veReNJFuoXwgPYMwB7HY2PW99N4F4OAxJA7wAJnFOo44p0j/b1IgWibz44PAwg9vTmXhNVyfLrEh9LzEjMSzRxSltzumpQ5+mwITErcdPzyFPkzl5JwIAQ0PLmp0IR0Lh1XaHoHiCyAAabAAFHx8f+kRg6CbCh6XnJjOc1IK+qZutrgVXCh0aAHbPtOu2VJTkBc0srYu7X+7kRwCFQaAKwodn5e09hcaOlsLzWFL+vr4tb8z+LxUbjtFzquvGLjWZPCMCzz9QD/nn0SL30wcM/FLYe7igSQADSJIDlqE/dRgCelZQA1M3bA9qemYEApAGQEEYASYjzAJKRxANQlwQEhpm3S/SvHgFtvQcEwAMWW1sKcQTY5gQajSt69btv7yjdtgroeTOa4fQA3fAgCbO+sZ1tSDkEdnb3/DF9LM/a6HmfhMaGNgdsRM4JOu7+8KXvAXTzY48UE7oHALoH6MZ3GnozMi8xATBcrQJyBQDY+8C9lVXP8A2lt9MNBehJVgpImPHEvOtY7wsjgkMgj7iB7664mjg8OlJl+lWH7aW6sdDjjA8jLeseJTEB7u6u4NWGqDphBOiIa+T+/n7AcN34paWlxFhYWBD1ej2AvhCQtudBAGAaD5hb3bRIRECYe+tunmYIdAMSYOafBQGllFJKKaWUUlyJ29md7SczpfTfA8ze7u9HVAkbl+XH14rFmLTpcMEx98+/9hSgx5XH1e8Bib2Vn5ZXfYOgd5Tf18qlbqb7PATyOQ4bB56AeMEQBkVsh6GBJ6CcZUsPKKV3PZY2uOrWHeFOS9TaX3sP4g8coQR4v/ikJqDlf5oz4AR06wH/ewKKPgQyzdLKYBMaAVbUvDp1RxSagLnl39Q3B/9pAhA+82OFXjgNsUbEFxFn7ChvbuZNQH9/L2hHiw8UoMNoBFYBElBREegDPwKt1w8joGL54NHPK5IH2AhA7zOdlgDfM2pOwGidEKVPO1l+IzgFGnEkt7SAF+/vMBAuDfD7AL2+aZCZjiPANieQgEC+Z7hepnlA978XqG8CvDieFyYPjnE5ZkkAvyEiASptmQOYRx0vQ5oE6B9cWCdGYwiYBLBOIYaA+S+xH1eWxfrWproCJAB5vAK/PFhT8Nb2ThJqdhc3icjjsMR/YFj/iWGUWYOjUd8XkADW6VVwtZ+nxcjwOgkIq2PG8wFdbOVp4/+xBmT1gCwE5BD+7r8HMK4PITHc+5MA1jEJ7Hf8/19xilpwHooa/QAAAABJRU5ErkJggg=="
    h = SHA1.new()
    c = pkcs1_15.new(privkey)
    h.update(skin.encode())
    skin_sig = c.sign(h)

    response = {
        "id": UUID(session.player.uuid).hex,
        "name": session.player.username,
        "properties": [
            {
                "name": "textures",
                "value": skin,
                "signarure": base64.b64encode(skin_sig).decode()
            }            
        ]
    }        

    req.app.logger.info(f"[{username}] joined game session with serverId: {serverId}")
    return JSONResponse(response)

@router.get("/blockedservers")
async def blockedservers(req: Request):
    req.app.logger.debug(f"/blockedservers (fck it)")
    return Response(status_code=204)   

@router.get("/session/minecraft/profile/{uuid}")
async def profile(req: Request, uuid: str):
    player = db.session.query(Player).where(Player.uuid == str(UUID(uuid))).first()

    if not player or not player.texture_id:
        req.app.logger.debug(f"Player with uuid {UUID(uuid)} doen't exist or have no texture!")
        return Response(status_code=404)
    
    encoded_texture = base64.b64encode(json.dumps(
        {
            'timestamp': 1678186617494,
            'profileId': uuid,
            'profileName': player.username,
            'textures': {
                'SKIN': {
                    'url': f'http://textures.minecraft.net/texture/{player.texture_id}',
                    'metadata': {'model': 'slim'}
                }
            }
        }  
    ).encode()).decode()

    response = {
        "id": uuid,
        "name": player.username,
        "properties": [
            {
            "name" : "textures",
            "value": encoded_texture
            }
        ]
    }

    return JSONResponse(response)    

@router.post("/session/minecraft/falsify")
async def falsify(req: Request, body: Falsify):
    """Spoof serverId for proto decryption"""
    if not req.app.allow_falsify:
        return Response(status_code=403)

    session = db.session.query(Session).where(Session.serverId == body.our).first()
    if not session:
        return Response(status_code=400)

    session.serverId = body.their
    db.session.commit()

    req.app.logger.info(f"Falsified game session with serverId proxy[{body.our}] -> server[{body.their}]")
    return Response(status_code=204)
