from fastapi import APIRouter, Request, Response, status
from fastapi.responses import JSONResponse

from pydantic import BaseModel
from typing import Optional

from fastapi_sqlalchemy import db

from models import Player, Token
import uuid
import datetime

router = APIRouter()

class AuthenticateRequestAgent(BaseModel):
    name: str
    version: str

class AuthenticateRequest(BaseModel):
    agent: AuthenticateRequestAgent
    username: str
    password: str
    clientToken: Optional[str] = None
    requestUser: str = 'False'

class TokenRequest(BaseModel):
    accessToken: str
    clientToken: str
    requestUser: str = 'False'  


@router.post("/authenticate")
async def authenticate(req: Request, body: AuthenticateRequest):
    '''Authenticate request'''
    player = db.session.query(Player).where(Player.username == body.username).first()
    if not player:
        if req.app.autoreg:
            player = Player(
                uuid = str(uuid.uuid4()),
                username = body.username,
                password = body.password,
                banned = False,
                singleplayer = False,
                preferredLanguage = req.app.autoreg_language,
                registrationCountry = req.app.autoreg_country,
                joined_at = datetime.datetime.now(),
                texture_id = req.app.autoreg_texture
            )
            db.session.add(player); db.session.commit()
            req.app.logger.debug(f"Autoregisted {player.username} [{player.uuid}]")
        else:
            response = {
                "error": "ForbiddenOperationException",
                "errorMessage": "Invalid credentials."
            }
            req.app.logger.debug(f"Attempt to login as non-existing user [{body.username}]")
            return JSONResponse(response, status_code=403)

    if player.password != body.password:
        response = {
            "error": "ForbiddenOperationException",
            "errorMessage": "Invalid credentials."
        }
        req.app.logger.debug(f"Attempt to login as [{body.username}] with invalid password")
        return JSONResponse(response, status_code=403)
                
    token = db.session.query(Token).where(Token.player_uuid == player.uuid).where(Token.clientToken == body.clientToken).first()
    
    if token:
        req.app.logger.debug(f"{player.username} logging in from the same device [{token.clientToken}] but old token [{token.accessToken}] still valid, invalidating...")
        db.session.delete(token)

    accessToken = uuid.uuid4().hex
    clientToken = body.clientToken if body.clientToken else str(uuid.uuid4())    

    token = Token(
        accessToken = accessToken,
        clientToken = clientToken,
        created_at = datetime.datetime.now(),
        player = player
    )

    db.session.add(token)
    db.session.commit()

    response = {
        "clientToken": clientToken,
        "accessToken": accessToken,
        "availableProfiles": [
            {
                "name": player.username,
                "id": uuid.UUID(token.player.uuid).hex
            }
        ],
        "selectedProfile": {
            "name": player.username,
            "id": uuid.UUID(token.player.uuid).hex
        }
    }
    user = {
        "properties": [
            {
                "name": "preferredLanguage",
                "value": player.preferredLanguage
            },
            {
                "name": "registrationCountry",
                "value": player.registrationCountry
            }
        ],
        "id": uuid.UUID(token.player.uuid).hex,
        "username": player.username
    }
    if body.requestUser != "False":
        response["user"] = user
    
    req.app.logger.debug(f"accessToken[{token.accessToken}] paired with clientToken[{token.clientToken}]")
    req.app.logger.info(f"[{player.username}] logged in")
    return JSONResponse(response)


@router.post("/validate")
async def validate(req: Request, body: TokenRequest):
    '''Token validate request'''
    req.app.logger.debug(f"/validate, {body}")
    token = db.session.query(Token).where(Token.accessToken == body.accessToken).where(Token.clientToken == body.clientToken).first()
    if not token:
        response = {
            "error": "ForbiddenOperationException",
            "errorMessage": "Invalid token."
        }        
        return JSONResponse(response, status_code=403)

    req.app.logger.debug(f"accessToken[{token.accessToken}] paired with clientToken[{token.clientToken}]")
    req.app.logger.info(f"[{token.player.username}] validated his token")

    if body.requestUser == "False":
        return Response(status_code=204)    

    response = {
        "user": {
            "properties": [
                {
                    "name": "preferredLanguage",
                    "value": token.player.preferredLanguage
                },
                {
                    "name": "registrationCountry",
                    "value": token.player.registrationCountry
                }
            ],
            "id": uuid.UUID(token.player.uuid).hex,
            "username": token.player.username
        }
    }

    return JSONResponse(response)

@router.post("/refresh")
async def refresh(req: Request, body: TokenRequest):
    '''Token refresh request'''
    token = db.session.query(Token).where(Token.accessToken == body.accessToken).where(Token.clientToken == body.clientToken).first()
    if not token:
        response = {
            "error": "ForbiddenOperationException",
            "errorMessage": "Invalid token."
        }
        return JSONResponse(response, status_code=403)

    db.session.delete(token)

    clientToken = body.clientToken if body.clientToken else str(uuid.uuid4())
    accessToken = str(uuid.uuid4())

    new_token = Token(
        accessToken = accessToken,
        clientToken = clientToken,
        created_at = datetime.datetime.now(),
        player = token.player
    )

    db.session.add(new_token)
    db.session.commit()
    response = {
        "accessToken": accessToken,
        "clientToken": clientToken,
        "selectedProfile": {
            "id": uuid.UUID(token.player.uuid).hex,
            "name": token.player.username
        }
    }
    user = {
        "properties": [
            {
                "name": "preferredLanguage",
                "value": token.player.preferredLanguage[:2]
            }
        ],
        "id": uuid.UUID(token.player.uuid).hex,
        "username": token.player.username
    }
    if body.requestUser != "False":
        response["user"] = user

    req.app.logger.debug(f"accessToken[{token.accessToken} -> {new_token.accessToken}] paired with clientToken[{token.clientToken}]")
    req.app.logger.info(f"[{token.player.username}] refreshed his token")
    return JSONResponse(response)

@router.post("/invalidate")
async def invalidate(req: Request):
    '''Token invalidate request'''
    """
        B. R. O. K. E. N.
    """
    #token = db.session.query(Token).where(Token.accessToken == body.accessToken).where(Token.clientToken == Token.clientToken).first()
    #if not token:
    #    response = {
    #        "error": "ForbiddenOperationException",
    #        "errorMessage": "Invalid token."
    #    }
    #    return JSONResponse(response, status_code=403)
    
    #db.session.delete(token)
    #db.session.commit()
    #print(f'[{token.player.username}] logged out')
    req.app.logger.debug(f"fuck you /invalidate, if it still empty here <3")
    invalid = await req.json()
    if invalid:
        print(invalid)
        req.app.logger.critical("once a blue moon, you saw it, report us IMMEDIATELY on gitlab how we can get the same results")

    return Response(status_code=204)     
